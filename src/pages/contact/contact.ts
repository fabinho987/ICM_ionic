import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController } from 'ionic-angular';
import leaflet from 'leaflet';

@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {

  @ViewChild('map') mapContainer: ElementRef;
  map: any;
  constructor(public navCtrl: NavController) {

  }

  ionViewDidEnter() {
    this.loadmap();
  }
 
  loadmap() {
    this.map = leaflet.map("map").fitWorld();
    leaflet.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attributions: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
      maxZoom: 20
    }).addTo(this.map);
    this.map.locate({
      setView: true,
      maxZoom: 20
    }).on('locationfound', (e) => {
      let markerGroup = leaflet.featureGroup();
      let marker: any = leaflet.marker([e.latitude, e.longitude]).on('click', () => {
        alert('You are here!');
      })
      let markerCrasto: any = leaflet.marker([40.624570, -8.656846]).on('click', () => {
        alert('Refectory Crasto');
      })
      let markerSantiago: any = leaflet.marker([40.630587,  -8.659250]).on('click', () => {
        alert('Refectory Santiago');
      })
      let markerSnack: any = leaflet.marker([40.631279, -8.655458]).on('click', () => {
        alert('Refectory Snack-bar');
      })
      markerGroup.addLayer(markerSnack);
      markerGroup.addLayer(markerCrasto);
      markerGroup.addLayer(marker);
      markerGroup.addLayer(markerSantiago);
      this.map.addLayer(markerGroup);
      }).on('locationerror', (err) => {
        alert(err.message);
    })
 
  }
 

}
