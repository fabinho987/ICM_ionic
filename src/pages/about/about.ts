import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { DetailsPage } from '../details/details';

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {
  notas: any;

  notes = [
      {
        "ref": "Refeitório Santiago",
        "type" : "Almoço",
        "pratoC": "Carne de vaca estufada com massa esparguete",
        "pratoP": "Bacalhau espiritual",
        "pratoO": "Creme de brócolos",
        "sopa": "Creme de brócolos",
        "sub": "Fruta da época ou doce",
        "data":"Fri, 22 Dec 2017"
      },
      {
        "ref": "Refeitório Santiago",
        "type" : "Almoço",
        "pratoC": "Filetes com arroz malandro",
        "pratoP": "Bacalhau a casa",
        "pratoO": "Creme de espinafres",
        "sopa": "Creme de brócolos",
        "sub": "Fruta da época ou doce",
        "data":"Sat, 23 Dec 2017"
      },
      {
        "ref": "Refeitório Santiago",
        "type" : "Almoço",
        "pratoC": "Carne de Franfo estufada com massa expiral",
        "pratoP": "Bacalhau com natas",
        "pratoO": "Caldo Verdes",
        "sopa": "Creme de brócolos",
        "sub": "Fruta da época ou doce",
        "data":"Sun, 24 Dec 2017"
      },
      {
         "ref": "Refeitório Santiago",
        "type" : "Almoço",
        "pratoC": "Carne de porco a chefe ",
        "pratoP": "Bacalhau",
        "pratoO": "Creme de ervilhas",
        "sopa": "Creme de brócolos",
        "sub": "Fruta da época ou doce",
        "data":"Mon, 25 Dec 2017"
      },
      {
        "ref": "Refeitório Santiago",
        "type" : "Almoço",
        "pratoC": "Carne de vaca estufada com massa esparguete",
        "pratoP": "Bacalhau espiritual",
        "pratoO": "Creme de brócolos",
        "sopa": "Creme de brócolos",
        "sub": "Fruta da época ou doce",
        "data":"Teu, 26 Dec 2017"
      }
]
  constructor(public navCtrl: NavController) {
    console.log(this.notes);
  }
    showDetails(note){
      this.navCtrl.push(DetailsPage, {note: note});
    }
}
